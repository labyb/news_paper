from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import DeleteView, UpdateView, CreateView
from django.urls import reverse_lazy
from .forms import PostForm
from . import models
from django.contrib.auth.mixins import LoginRequiredMixin

class ArticleListView(LoginRequiredMixin, ListView):
    model = models.Article
    template_name = 'article_list.html'
    login_url = 'login'

class ArticleEditView(LoginRequiredMixin, UpdateView):
    model = models.Article
    form_class = PostForm
    template_name = 'article_edit.html'
    login_url = 'login'

class ArticleDetailView(LoginRequiredMixin, DetailView):
    model = models.Article
    template_name = 'article_detail.html'
    login_url = 'login'

class ArticleDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Article
    template_name = 'article_delete.html'
    success_url = reverse_lazy('article_list')
    login_url = 'login'
class ArticleCreateView(LoginRequiredMixin, CreateView):
    model = models.Article
    form_class = PostForm
    template_name = 'article_create.html'
    success_url = reverse_lazy('article_list')
    login_url = 'login'

    def form_valid(self, form):

        form.instance.author = self.request.user
        return super().form_valid(form)